# web-temp

A web app temperature gauge that uses the NodeMCU & BME280 Bosch Sensortec temperature sensor.

## Setup

Expects the BME280 sensor to be attached to `sda, scl = 3, 4` or `GPIO0 & GPIO2` as shown on the board

Requires firmware with the following modules
- LUA_USE_MODULES_ADC
- LUA_USE_MODULES_BIT
- LUA_USE_MODULES_BME280
- LUA_USE_MODULES_DHT
- LUA_USE_MODULES_FILE
- LUA_USE_MODULES_GPIO
- LUA_USE_MODULES_I2C
- LUA_USE_MODULES_MQTT
- LUA_USE_MODULES_NET
- LUA_USE_MODULES_NODE
- LUA_USE_MODULES_OW
- LUA_USE_MODULES_SPI
- LUA_USE_MODULES_TMR
- LUA_USE_MODULES_UART
- LUA_USE_MODULES_WIFI

You also need to add in your router settings on lines 76 & 77 of `init.lua`.

## Install

Flash both index.html and init.lua to your NodeMCU using the following [guide](https://nodemcu.readthedocs.io/en/master/upload/).

## Usage

Once installed and the NodeMcu has rebooted use either a terminal connection or your router to discover the devices IP address. If you navigate to this address in your browser you should now get a temperature reading that is updated every 10 seconds.

## Known issues

The BME280 unit is very sensative, if you install it too closely to the NodeMcu the sensor will report values ~2 degrees Celsius warmer than the actual temperature.
