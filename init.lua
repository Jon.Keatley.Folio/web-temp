------------------------------------------------------------------------
function get_temp()
  T = bme280.temp()
  local Tsgn = (T < 0 and -1 or 1); T = Tsgn*T
  return string.format("%s%d.%02d", Tsgn<0 and "-" or "", T/100, T%100)
end
------------------------------------------------------------------------
function wifi_got_ip_event(T)
  print("Connected on " .. T.IP)
end
------------------------------------------------------------------------
function send_http_response(conn,msg,content_type)
  local buffer = "HTTP/1.1 200 OK\r\n"
  buffer = buffer .. "Content-Length" .. string.len(msg) .. "\r\n"
  buffer = buffer .. "Content-Type: " .. content_type .. "\r\n"
  buffer = buffer .. "Connection: Closed\r\n"
  buffer = buffer .. "\r\n\r\n" .. msg

  local function sent(localSocket)
    localSocket:close()
    print("Response sent")
  end

  -- triggers the send() function again once the first chunk of data was sent
  conn:on("sent", sent)
  conn:send(buffer)
end
------------------------------------------------------------------------
function send_file_http_response(conn,path,content_type)

	local function sent(localSocket)
		localSocket:close()
		print("Response sent")
	end

	-- triggers the send() function again once the first chunk of data was sent
	conn:on("sent", sent)

	local buffer = "HTTP/1.1 200 OK\r\n"

	if file.open(path,"r") then
		local stats = file.stat(path)
		buffer = buffer ..  "Content-Length" .. stats.size .. "\r\n"
		buffer = buffer ..  "Content-Type: " .. content_type .. "\r\n"
		buffer = buffer ..  "Connection: Closed\r\n"
		buffer = buffer ..  "\r\n\r\n"
		conn:send(buffer)

		local line = file.readline()
		while line ~= nil do
			conn:send(line)
			line = file.readline()
		end
		file.close()

	else
		local errormsg = "File not found"
		buffer = buffer ..  "Content-Length" .. string.len(errormsg) .. "\r\n"
		buffer = buffer ..  "Content-Type: " .. content_type .. "\r\n"
		buffer = buffer ..  "Connection: Closed\r\n"
		buffer = buffer ..  "\r\n\r\n" .. errormsg
		conn:send(buffer)
	end
end


------------------------------------------------------------------------
------------------------------------------------------------------------
-- delayed init
local init = tmr.create()
init:register(1000,tmr.ALARM_SINGLE,function ()
	sda, scl = 3, 4
	i2c.setup(0, sda, scl, i2c.SLOW) -- call i2c.setup() only once
	bme280.setup()

	local SSID = "SSID"
	local SSID_PASSWORD = "password"

	-- configure ESP as a station
	print("starting")
	wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, wifi_got_ip_event)
	wifi.setmode(wifi.STATION)
	wifi.sta.config({
	ssid =SSID,
	pwd = SSID_PASSWORD,
	auto = true,
	save = false,
	})

	-- configure web server
	srv=net.createServer(net.TCP)
	srv:listen(80,function(conn)
    conn:on("receive",function(conn,payload)
    print(payload)
	validPath = false
    for s in payload:gmatch("[^\r\n]+") do
		if s:sub(1,3) == "GET" then
			path = s:sub(5,s:len())
			if path:find(' ') then
				local s,e = path:find(' ')
				path = path:sub(1,s -1)
			end

			if (path == "/") then
				send_file_http_response(conn,"index.html","text/html")
				validPath = true
			elseif (path == "/temp.json") then
				send_http_response(conn,"{\"temp\":\"".. get_temp() .."\"}","application/json")
				validPath = true
			end
		end
	end

	if validPath == false then
		send_http_response(conn,"<html><head><title>Error</title></head><body><h1>Incorrect Usage!</h1></body></html>","text/html")
	end

    end)
end)

	--poll:start()
	init:unregister()
end)
init:start()
